package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Activities:
{
  "type": "object",
  "properties": {
    "offset": {
      "description": "Position in pagination.",
      "type": "integer",
      "format": "int32"
    },
    "limit": {
      "description": "Number of items to retrieve (100 max).",
      "type": "integer",
      "format": "int32"
    },
    "count": {
      "description": "Total number of items available.",
      "type": "integer",
      "format": "int32"
    },
    "history": {
      "type": "array",
      "items": {
        "$ref": "Activity"
      }
    }
  }
}
*/

public class Activities {

	@Size(max=1)
	private Integer offset;

	@Size(max=1)
	private Integer limit;

	@Size(max=1)
	private Integer count;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Activity> history;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    
	    
	    history = new ArrayList<com.digitalml.rest.resources.codegentest.Activity>();
	}
	public Integer getOffset() {
		return offset;
	}
	
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public Integer getLimit() {
		return limit;
	}
	
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getCount() {
		return count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
	public List<com.digitalml.rest.resources.codegentest.Activity> getHistory() {
		return history;
	}
	
	public void setHistory(List<com.digitalml.rest.resources.codegentest.Activity> history) {
		this.history = history;
	}
}