package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Error:
{
  "type": "object",
  "properties": {
    "code": {
      "type": "integer",
      "format": "int32"
    },
    "message": {
      "type": "string"
    },
    "fields": {
      "type": "string"
    }
  }
}
*/

public class Error {

	@Size(max=1)
	private Integer code;

	@Size(max=1)
	private String message;

	@Size(max=1)
	private String fields;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    message = null;
	    fields = null;
	}
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFields() {
		return fields;
	}
	
	public void setFields(String fields) {
		this.fields = fields;
	}
}