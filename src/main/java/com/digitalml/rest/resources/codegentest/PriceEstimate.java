package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for PriceEstimate:
{
  "type": "object",
  "properties": {
    "product_id": {
      "description": "Unique identifier representing a specific product for a given latitude & longitude. For example, uberX in San Francisco will have a different product_id than uberX in Los Angeles",
      "type": "string"
    },
    "currency_code": {
      "description": "[ISO 4217](http://en.wikipedia.org/wiki/ISO_4217) currency code.",
      "type": "string"
    },
    "display_name": {
      "description": "Display name of product.",
      "type": "string"
    },
    "estimate": {
      "description": "Formatted string of estimate in local currency of the start location. Estimate could be a range, a single number (flat rate) or \"Metered\" for TAXI.",
      "type": "string"
    },
    "low_estimate": {
      "description": "Lower bound of the estimated price.",
      "type": "string"
    },
    "high_estimate": {
      "description": "Upper bound of the estimated price.",
      "type": "string"
    },
    "surge_multiplier": {
      "description": "Expected surge multiplier. Surge is active if surge_multiplier is greater than 1. Price estimate already factors in the surge multiplier.",
      "type": "string"
    }
  }
}
*/

public class PriceEstimate {

	@Size(max=1)
	private String product_id;

	@Size(max=1)
	private String currency_code;

	@Size(max=1)
	private String display_name;

	@Size(max=1)
	private String estimate;

	@Size(max=1)
	private String low_estimate;

	@Size(max=1)
	private String high_estimate;

	@Size(max=1)
	private String surge_multiplier;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    product_id = null;
	    currency_code = null;
	    display_name = null;
	    estimate = null;
	    low_estimate = null;
	    high_estimate = null;
	    surge_multiplier = null;
	}
	public String getProduct_id() {
		return product_id;
	}
	
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getDisplay_name() {
		return display_name;
	}
	
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	public String getEstimate() {
		return estimate;
	}
	
	public void setEstimate(String estimate) {
		this.estimate = estimate;
	}
	public String getLow_estimate() {
		return low_estimate;
	}
	
	public void setLow_estimate(String low_estimate) {
		this.low_estimate = low_estimate;
	}
	public String getHigh_estimate() {
		return high_estimate;
	}
	
	public void setHigh_estimate(String high_estimate) {
		this.high_estimate = high_estimate;
	}
	public String getSurge_multiplier() {
		return surge_multiplier;
	}
	
	public void setSurge_multiplier(String surge_multiplier) {
		this.surge_multiplier = surge_multiplier;
	}
}