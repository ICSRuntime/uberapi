package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Product:
{
  "type": "object",
  "properties": {
    "product_id": {
      "description": "Unique identifier representing a specific product for a given latitude & longitude. For example, uberX in San Francisco will have a different product_id than uberX in Los Angeles.",
      "type": "string"
    },
    "description": {
      "description": "Description of product.",
      "type": "string"
    },
    "display_name": {
      "description": "Display name of product.",
      "type": "string"
    },
    "capacity": {
      "description": "Capacity of product. For example, 4 people.",
      "type": "string"
    },
    "image": {
      "description": "Image URL representing the product.",
      "type": "string"
    }
  }
}
*/

public class Product {

	@Size(max=1)
	private String product_id;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private String display_name;

	@Size(max=1)
	private String capacity;

	@Size(max=1)
	private String image;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    product_id = null;
	    description = null;
	    display_name = null;
	    capacity = null;
	    image = null;
	}
	public String getProduct_id() {
		return product_id;
	}
	
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisplay_name() {
		return display_name;
	}
	
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	public String getCapacity() {
		return capacity;
	}
	
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
}