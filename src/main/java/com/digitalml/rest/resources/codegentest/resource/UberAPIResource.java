package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.UberAPIService;
	
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveHistoryByOffsetReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveHistoryByOffsetReturnDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveHistoryByOffsetInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesPriceByStart_latitudeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesPriceByStart_latitudeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesPriceByStart_latitudeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveMeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveMeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveMeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesTimeByStart_latitudeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesTimeByStart_latitudeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesTimeByStart_latitudeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveProductsByLatitudeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveProductsByLatitudeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveProductsByLatitudeInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Uber API
	 * Move your app forward with the Uber APIa
	 *
	 * @author admin
	 * @version 1.0.0
	 *
	 */
	
	@Path("https://api.uber.com/v1")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class UberAPIResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(UberAPIResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private UberAPIService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private UberAPIService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof UberAPIService)) {
			LOGGER.error(implementationClass + " is not an instance of " + UberAPIService.class.getName());
			return null;
		}

		return (UberAPIService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: retrieveHistoryByOffset
		The User Activity endpoint returns data about a user&#x27;s lifetime activity with
Uber. The response will include pickup locations and times, dropoff locations
and times, the distance of past requests, and information about which products
were requested.&lt;br&gt;&lt;br&gt;The history array in the response will have a maximum
length based on the limit parameter. The response value count may exceed limit,
therefore subsequent API requests may be necessary.

	Non-functional requirements:
	*/
	
	@GET
	@Path("/history")
	public javax.ws.rs.core.Response retrieveHistoryByOffset(
		@QueryParam("offset") Integer offset,
		@QueryParam("limit") Integer limit) {

		RetrieveHistoryByOffsetInputParametersDTO inputs = new UberAPIService.RetrieveHistoryByOffsetInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setLimit(limit);
	
		try {
			RetrieveHistoryByOffsetReturnDTO returnValue = delegateService.retrieveHistoryByOffset(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveEstimatesPriceByStartlatitude
		The Price Estimates endpoint returns an estimated price range
for each product
offered at a given location. The price estimate is
provided as a formatted
string with the full price range and the localized
currency symbol.&lt;br&gt;&lt;br&gt;The
response also includes low and high estimates,
and the [ISO
4217](http://en.wikipedia.org/wiki/ISO_4217) currency code for
situations
requiring currency conversion. When surge is active for a particular
product,
its surge_multiplier will be greater than 1, but the price estimate
already
factors in this multiplier.


	Non-functional requirements:
	*/
	
	@GET
	@Path("/estimates/price")
	public javax.ws.rs.core.Response retrieveEstimatesPriceByStartlatitude(
		@QueryParam("start_latitude")@NotEmpty String start_latitude,
		@QueryParam("start_longitude")@NotEmpty String start_longitude,
		@QueryParam("end_latitude")@NotEmpty String end_latitude,
		@QueryParam("end_longitude")@NotEmpty String end_longitude) {

		RetrieveEstimatesPriceByStart_latitudeInputParametersDTO inputs = new UberAPIService.RetrieveEstimatesPriceByStart_latitudeInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setStart_latitude(start_latitude);
		inputs.setStart_longitude(start_longitude);
		inputs.setEnd_latitude(end_latitude);
		inputs.setEnd_longitude(end_longitude);
	
		try {
			RetrieveEstimatesPriceByStart_latitudeReturnDTO returnValue = delegateService.retrieveEstimatesPriceByStartlatitude(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveMe
		The User Profile endpoint returns information about the Uber user that has
authorized with the application.

	Non-functional requirements:
	*/
	
	@GET
	@Path("/me")
	public javax.ws.rs.core.Response retrieveMe() {

		RetrieveMeInputParametersDTO inputs = new UberAPIService.RetrieveMeInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveMeReturnDTO returnValue = delegateService.retrieveMe(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveEstimatesTimeByStartlatitude
		The Time Estimates endpoint returns ETAs for all products offered at a given
location, with the responses expressed as integers in seconds. We recommend that
this endpoint be called every minute to provide the most accurate, up-to-date
ETAs.

	Non-functional requirements:
	*/
	
	@GET
	@Path("/estimates/time")
	public javax.ws.rs.core.Response retrieveEstimatesTimeByStartlatitude(
		@QueryParam("start_latitude")@NotEmpty String start_latitude,
		@QueryParam("start_longitude")@NotEmpty String start_longitude,
		@QueryParam("customer_uuid") String customer_uuid,
		@QueryParam("product_id") String product_id) {

		RetrieveEstimatesTimeByStart_latitudeInputParametersDTO inputs = new UberAPIService.RetrieveEstimatesTimeByStart_latitudeInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setStart_latitude(start_latitude);
		inputs.setStart_longitude(start_longitude);
		inputs.setCustomer_uuid(customer_uuid);
		inputs.setProduct_id(product_id);
	
		try {
			RetrieveEstimatesTimeByStart_latitudeReturnDTO returnValue = delegateService.retrieveEstimatesTimeByStartlatitude(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveProductsByLatitude
		The Products endpoint returns information about the *Uber* products
offered at a
given location. The response includes the display name
and other details about
each product, and lists the products in the
proper display order.


	Non-functional requirements:
	*/
	
	@GET
	@Path("/products")
	public javax.ws.rs.core.Response retrieveProductsByLatitude(
		@QueryParam("latitude")@NotEmpty String latitude,
		@QueryParam("longitude")@NotEmpty String longitude) {

		RetrieveProductsByLatitudeInputParametersDTO inputs = new UberAPIService.RetrieveProductsByLatitudeInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLatitude(latitude);
		inputs.setLongitude(longitude);
	
		try {
			RetrieveProductsByLatitudeReturnDTO returnValue = delegateService.retrieveProductsByLatitude(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}