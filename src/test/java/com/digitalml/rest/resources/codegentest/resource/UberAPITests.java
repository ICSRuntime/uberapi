package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class UberAPITests {

	@Test
	public void testResourceInitialisation() {
		UberAPIResource resource = new UberAPIResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationRetrieveHistoryByOffsetNoSecurity() {
		UberAPIResource resource = new UberAPIResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveHistoryByOffset(0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveEstimatesPriceByStart_latitudeNoSecurity() {
		UberAPIResource resource = new UberAPIResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveEstimatesPriceByStartlatitude(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveMeNoSecurity() {
		UberAPIResource resource = new UberAPIResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveMe();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveEstimatesTimeByStart_latitudeNoSecurity() {
		UberAPIResource resource = new UberAPIResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveEstimatesTimeByStartlatitude(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveProductsByLatitudeNoSecurity() {
		UberAPIResource resource = new UberAPIResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveProductsByLatitude(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}