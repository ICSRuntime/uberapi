package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesPriceByStart_latitudeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesPriceByStart_latitudeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveEstimatesPriceByStart_latitudeTests {

	@Test
	public void testOperationRetrieveEstimatesPriceByStart_latitudeBasicMapping()  {
		UberAPIServiceDefaultImpl serviceDefaultImpl = new UberAPIServiceDefaultImpl();
		RetrieveEstimatesPriceByStart_latitudeInputParametersDTO inputs = new RetrieveEstimatesPriceByStart_latitudeInputParametersDTO();
		inputs.setStart_latitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setStart_longitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setEnd_latitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setEnd_longitude(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveEstimatesPriceByStart_latitudeReturnDTO returnValue = serviceDefaultImpl.retrieveEstimatesPriceByStartlatitude(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}